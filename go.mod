module bitbucket.org/aisgteam/ai-api-client

go 1.12

require (
	bitbucket.org/aisgteam/ai-struct v0.0.9
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
)
